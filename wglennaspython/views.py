from flask import Blueprint, render_template

views = Blueprint('views', __name__)


@views.route('/')
@views.route('/home')
def home():
    return render_template('index.html')


@views.route('/python')
def python_page():
    return render_template(
        'python.html',
    )


@views.route('/store')
def store_page():
    return render_template('store.html')


@views.route('/blogs_honeybee')
def blogs_honeybee_page():
    return render_template('blogs_honeybee.html')


@views.route('/blogs_christian')
def blogs_christian_page():
    return render_template('blogs_christian.html')
